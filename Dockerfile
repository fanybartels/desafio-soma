# Imagem de Base httpd:2.4
# ----------------------------------------------
FROM httpd:2.4
# ----------------------------------------------
# Primeira Camada (Layer) copia o arquivo com o Form HTML para o diretorio 
# DocumentRoot mapeado no apache, neste caso o /usr/local/apache2/htdocs
# renomenado no destino para index.html.
# ----------------------------------------------
COPY soma.html /usr/local/apache2/htdocs/index.html
# ----------------------------------------------
# Segunda Camada (Layer) copia o arquivo shell script (soma.cgi) para o 
# diretorio padrão da imagem para CGI (/usr/local/apache2/cgi-bin).
# ----------------------------------------------
COPY soma.cgi /usr/local/apache2/cgi-bin/soma.cgi
# ----------------------------------------------
# Link de Referencia para habilitar o módulo de CGI no Apache2 (HTTPD)
# https://stackoverflow.com/questions/64743879/docker-httpd-apache-and-getting-cgi-bin-to-execute-perl-script
# ----------------------------------------------
RUN chmod +x /usr/local/apache2/cgi-bin/soma.cgi
CMD httpd-foreground -c "LoadModule cgid_module modules/mod_cgid.so"
